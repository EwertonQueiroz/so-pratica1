CFLAGS = -lpthread
CFLAGS += -std=c99

test: main
	@./main.out

main: main.c
	@echo Compiling $@
	@$(CC) $(CFLAGS) main.c -o main.out

clean:
	@echo "Cleaning up"
	rm -rf *.out
