#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#define NUM_THREADS 5

void *p (void *threadId);
void lock (int *threadId);
void unlock (int *threadId);

int str = -1;

int flag[] = {0, 0, 0, 0, 0};
int turn = 1;

int main ()
{
	pthread_t threads[NUM_THREADS];
	int err[NUM_THREADS];

	for (int i = 0; i < NUM_THREADS; i++)
	{
		printf("[!] Creating thread %d\n", i);
		err[i] = pthread_create(&threads[i], NULL, p, (void *)i);

		if (err[i])
		{
			printf("An error occurred in thread[%d]: pthread_create() returned %d\n", i, err[i]);
			return 1;
		}
	}

	for (int i = 0; i < NUM_THREADS; i++)
		pthread_join(threads[i], NULL);
	

	printf("%d\n", str);

	return 0;
}

void *p (void *threadId)
{
	int *tId;
	tId = (int *)threadId;

	printf("[+] Thread %d is entering critical section\n", tId);
	lock(tId);
	sleep(5);
	str = tId;
	printf("str: %d\n", tId);

	unlock(tId);
	printf("[-] Thread %d is exiting critical section\n", tId);

	pthread_exit(NULL);
}

void lock (int *threadId)
{
	printf("Into LOCK for %d\n", (int)threadId);
	flag[(int)threadId] = 1;

	turn = ((int)threadId + 1) % NUM_THREADS;
	printf("flag[%d]: %d\nturn: %d\n", (int)threadId, flag[(int)threadId], turn);
	while (flag[turn] && turn == ((int)threadId + 1) % NUM_THREADS)
		printf("[E] Thread %d is trying to access the critical section\n", (int)threadId);
}

void unlock (int *threadId)
{
	printf("Into UNLOCK for %d\n", (int)threadId);
	flag[(int)threadId] = 0;
}
